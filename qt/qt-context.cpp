#include <QApplication>

#include "main-window.hpp"
#include "opengl-widget.hpp"

int main(int argc, char *argv[])
{
	QApplication qApplication(argc, argv);
	MainWindow mainWindow;
	OpenglWidget openglWidget(&mainWindow);
	// Widget *widget = new Widget(&qMainWindow);
	// QWidget qWidget;
	// QOpenGLWidget qOpenGlWidget;

	// widget.setFormat(qSurfaceFormat);
	// widget.show();

	openglWidget.show();

	return qApplication.exec();
}
