#include "opengl-widget.hpp"

OpenglWidget::OpenglWidget(MainWindow *parent)
{
	QSurfaceFormat qSurfaceFormat;

	qSurfaceFormat.setDepthBufferSize(24);
	qSurfaceFormat.setVersion(3, 3);
	qSurfaceFormat.setProfile(QSurfaceFormat::CoreProfile);
	QSurfaceFormat::setDefaultFormat(qSurfaceFormat);
	qOpenGLContext = new QOpenGLContext;

	qOpenGLContext->setFormat(qSurfaceFormat);
	qOpenGLContext->create();
	qOpenGLContext->makeCurrent(qSurface);

	qOpenGLFunctions = qOpenGLContext->functions();
}

OpenglWidget::~OpenglWidget()
{

}

void OpenglWidget::initializeGL()
{}

void OpenglWidget::resizeGL(int w, int h)
{}

void OpenglWidget::paintGL()
{
	glClearColor(1.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT);
}

void OpenglWidget::paintEvent(QPaintEvent *e)
{}

void OpenglWidget::resizeEvent(QResizeEvent *e)
{}

bool OpenglWidget::event(QEvent *e)
{
	return true;
}
