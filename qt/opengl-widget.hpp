#ifndef OPENGL_WIDGET_HPP
#define OPENGL_WIDGET_HPP

#include <QOpenGLContext>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>

#include "main-window.hpp"

class OpenglWidget : public QOpenGLWidget {
	Q_OBJECT;
	QOpenGLContext *qOpenGLContext;
	QOpenGLFunctions *qOpenGLFunctions;
	QSurface *qSurface;
public:
	OpenglWidget(MainWindow *parent = nullptr);
	~OpenglWidget();
protected:
    virtual void initializeGL();
    virtual void resizeGL(int w, int h);
    virtual void paintGL();
    void paintEvent(QPaintEvent *e);
    void resizeEvent(QResizeEvent *e);
    bool event(QEvent *e);
};

#endif
