CC = gcc

CFLAGS = -g # -Wall -Wpedantic -Wextra

all:
	$(CC) main.c -o simple-window \
	gles/gles.c \
	vulkan/vulkan.c \
	wayland-client/egl/egl.c \
	wayland-client/wayland-client.c \
	wayland-client/presentation-time-client-protocol/presentation-time-client-protocol.c \
	wayland-client/viewporter-client-protocol/viewporter-client-protocol.c \
	wayland-client/xdg-decoration-unstable-v1-client-protocol/xdg-decoration-unstable-v1-client-protocol.c \
	wayland-client/xdg-shell-client-protocol/xdg-shell-client-protocol.c \
	$(CFLAGS) $(shell pkg-config --cflags --libs egl glesv2 vulkan wayland-client wayland-cursor wayland-egl)

clean:
	rm wayland-client
