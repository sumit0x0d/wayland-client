#ifndef SIMPLE_WINDOW_VULKAN_H
#define SIMPLE_WINDOW_VULKAN_H

#include <stdint.h>

void SW_Vulkan_Create(const char *applicationName, struct wl_display *wlDisplay, struct wl_surface *wlSurface,
	uint32_t width, uint32_t height);
void SW_Vulkan_Destroy();

void SW_Vulkan_Render();

#endif
