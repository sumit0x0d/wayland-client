#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define VK_USE_PLATFORM_WAYLAND_KHR
#define VK_ENABLE_BETA_EXTENSIONS

#include <vulkan/vulkan_core.h>
#include <vulkan/vulkan_wayland.h>
#include <wayland-client-protocol.h>

#include "vulkan.h"

#define MAJOR 1
#define MINOR 0
#define PATCH 0

typedef struct {
	VkInstance vkInstance;
	VkSurfaceKHR vkSurface;
	VkSwapchainKHR vkSwapchain;
	VkPhysicalDevice vkPhysicalDevice;
	VkDevice vkDevice;
	VkQueue vkQueue;
	VkCommandBuffer vkCommandBuffer;
	VkCommandPool vkCommandPool;
	VkImageView vkImageView;
	VkSemaphore vkSemaphore1;
	VkSemaphore vkSemaphore2;
	VkDeviceMemory vkDeviceMemory;
	VkImage vkImage;
	VkFramebuffer vkFramrbuffer;

	VkImage *swapchainImages;
} GlobalContext;

static GlobalContext global;



void SW_Vulkan_Create(const char *applicationName, struct wl_display *wlDisplay, struct wl_surface *wlSurface,
	uint32_t width, uint32_t height)
{
	VkResult vkResult = 0;
	const char *deviceExtensionNames[] = {
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
	};
	const char *instanceLayerNames[] = {
		"VK_LAYER_MESA_device_select",
		"VK_LAYER_KHRONOS_validation"
	};
	const char *instanceExtensionNames[] = {
		VK_KHR_SURFACE_EXTENSION_NAME,
		VK_KHR_WAYLAND_SURFACE_EXTENSION_NAME
	};

	VkApplicationInfo vkApplicationInfo = {
		.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
		.pNext = NULL,
		.pApplicationName = applicationName,
		.applicationVersion = 1,
		.pEngineName = NULL,
		.engineVersion = 0,
		.apiVersion = 0
	};
	VkInstanceCreateInfo vkInstanceCreateInfo = {
		.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.pApplicationInfo = &vkApplicationInfo,
		.enabledLayerCount = 2,
		.ppEnabledLayerNames = instanceLayerNames,
		.enabledExtensionCount = 2,
		.ppEnabledExtensionNames = instanceExtensionNames,
	};
	vkResult = vkCreateInstance(&vkInstanceCreateInfo, NULL, &global.vkInstance);
	assert(vkResult == 0);

	VkWaylandSurfaceCreateInfoKHR vkWaylandSurfaceCreateInfo = {
		.sType = VK_STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR,
		.pNext = NULL,
		.flags = 0,
		.display = wlDisplay,
		.surface = wlSurface
	};
	vkResult = vkCreateWaylandSurfaceKHR(global.vkInstance, &vkWaylandSurfaceCreateInfo, NULL, &global.vkSurface);
	assert(vkResult == 0);

	float deviceQueuePriority = 1.0f;
	VkDeviceQueueCreateInfo vkDeviceQueueCreateInfo = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.queueFamilyIndex = 0,
		.queueCount = 1,
		.pQueuePriorities = &deviceQueuePriority
	};
	VkDeviceCreateInfo vkDeviceCreateInfo = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.queueCreateInfoCount = 1,
		.pQueueCreateInfos = &vkDeviceQueueCreateInfo,
		.enabledLayerCount = 0,
		.ppEnabledLayerNames = NULL,
		.enabledExtensionCount = 1,
		.ppEnabledExtensionNames = deviceExtensionNames,
		.pEnabledFeatures = NULL
	};

	uint32_t physicalDeviceCount = 0;
	VkPhysicalDevice *vkPhysicalDevices = NULL;

	vkResult = vkEnumeratePhysicalDevices(global.vkInstance, &physicalDeviceCount, NULL);
	assert(vkResult == 0);
	vkPhysicalDevices = (VkPhysicalDevice *)malloc(physicalDeviceCount * sizeof (VkPhysicalDevice));
	assert(vkPhysicalDevices);
	vkResult = vkEnumeratePhysicalDevices(global.vkInstance, &physicalDeviceCount, vkPhysicalDevices);
	assert(vkResult == 0);

	VkPhysicalDeviceProperties vkDeviceProperties;
	for (int i = 0; i < physicalDeviceCount; i++) {
		vkGetPhysicalDeviceProperties(vkPhysicalDevices[i], &vkDeviceProperties);
		if (vkDeviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU) {
			global.vkPhysicalDevice = vkPhysicalDevices[i];
			vkDeviceQueueCreateInfo.queueFamilyIndex = i;
			break;
		}
	}

	free(vkPhysicalDevices);

	vkResult = vkCreateDevice(global.vkPhysicalDevice, &vkDeviceCreateInfo, NULL, &global.vkDevice);
	assert(vkResult == 0);

	vkGetDeviceQueue(global.vkDevice, vkDeviceQueueCreateInfo.queueFamilyIndex, 0, &global.vkQueue);

	VkSurfaceCapabilitiesKHR vkSurfaceCapabilities;
	vkResult = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(global.vkPhysicalDevice, global.vkSurface,
		&vkSurfaceCapabilities);
	assert(vkResult == 0);

	VkSwapchainCreateInfoKHR vkSwapchainCreateInfo = {
		.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
		.pNext = NULL,
		.flags = 0,
		.surface = global.vkSurface,
		.minImageCount = vkSurfaceCapabilities.minImageCount,
		.imageFormat = VK_FORMAT_B8G8R8A8_SRGB,
		.imageColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR,
		.imageExtent = {
			.width = width,
			.height = height
		},
		.imageArrayLayers = vkSurfaceCapabilities.maxImageArrayLayers,
		.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
		// imageSharingMode = ,
		// queueFamilyIndexCount = ,
		// pQueueFamilyIndices = ,
		.preTransform = vkSurfaceCapabilities.currentTransform,
		.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
		.presentMode = VK_PRESENT_MODE_FIFO_KHR
		// clipped = ,
		// oldSwapchain = 
	};
	vkResult = vkCreateSwapchainKHR(global.vkDevice, &vkSwapchainCreateInfo, NULL, &global.vkSwapchain);
	assert(vkResult == 0);

	uint32_t swapchainImageCount = 0;
	vkResult = vkGetSwapchainImagesKHR(global.vkDevice, global.vkSwapchain, &swapchainImageCount, NULL);
	assert(vkResult == 0);
	global.swapchainImages = (VkImage *)malloc(swapchainImageCount * sizeof (VkImage));
	assert(global.swapchainImages);
	vkResult = vkGetSwapchainImagesKHR(global.vkDevice, global.vkSwapchain, &swapchainImageCount,
		global.swapchainImages);
	assert(vkResult == 0);

	// vkImageCreateInfo.extent.width = width;
	// vkImageCreateInfo.extent.height = height;
	// result = vkCreateImage(vkDevice, &vkImageCreateInfo, NULL, &vulkan->swapchainImages[0]);
	// assert(result == 0);
	VkCommandPoolCreateInfo vkCommandPoolCreateInfo = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.queueFamilyIndex = vkDeviceQueueCreateInfo.queueFamilyIndex
	};
	vkResult = vkCreateCommandPool(global.vkDevice, &vkCommandPoolCreateInfo, NULL, &global.vkCommandPool);
	assert(vkResult == 0);

	VkSemaphoreCreateInfo vkSemaphoreCreateInfo = {
		.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0
	};
	vkCreateSemaphore(global.vkDevice, &vkSemaphoreCreateInfo, NULL, &global.vkSemaphore1);
	vkCreateSemaphore(global.vkDevice, &vkSemaphoreCreateInfo, NULL, &global.vkSemaphore2);

	// SW_Vulkan_Render(vulkan);
	// result = vkCreateImageView(vkDevice, &vkImageViewCreateInfo, NULL, &vkImageView);
	// assert(result == 0);
}

void SW_Vulkan_Destroy()
{
	vkDestroySwapchainKHR(global.vkDevice, global.vkSwapchain, NULL);
	vkDestroySurfaceKHR(global.vkInstance, global.vkSurface, NULL);
	vkDestroyDevice(global.vkDevice, NULL);
	vkDestroyInstance(global.vkInstance, NULL);
}

void SW_Vulkan_Render()
{
	uint32_t imageIndex = 0;
	VkResult vkResult;
	VkPipelineStageFlags waitDstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

	// VkAcquireNextImageInfoKHR x = {
	// .}
	vkResult = vkAcquireNextImageKHR(global.vkDevice, global.vkSwapchain, 0, global.vkSemaphore1, 0, &imageIndex);

	VkCommandBufferAllocateInfo vkCommandBufferAllocateInfo = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		.pNext = NULL,
		.commandPool = global.vkCommandPool,
		.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
		.commandBufferCount = 1
	};
	vkResult = vkAllocateCommandBuffers(global.vkDevice, &vkCommandBufferAllocateInfo, &global.vkCommandBuffer);

	VkCommandBufferBeginInfo vkCommandBufferBeginInfo = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
		.pNext = NULL,
		.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
		.pInheritanceInfo = NULL
	};
	vkResult = vkBeginCommandBuffer(global.vkCommandBuffer, &vkCommandBufferBeginInfo);

	VkClearColorValue color = {1,0,0,1};
	VkImageSubresourceRange range = {
		.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
		.layerCount = 1,
		.levelCount = 1
	};
	// VkDeviceMemory vkDeviceMemory;
	// vkBindImageMemory(vkDevice, vulkan->swapchainImages[imageIndex], vkDeviceMemory, 12) ;
	vkCmdClearColorImage(global.vkCommandBuffer, global.swapchainImages[imageIndex],
		VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, &color, 1, &range);
	vkResult = vkEndCommandBuffer(global.vkCommandBuffer);

	VkSubmitInfo vkSubmitInfo = {
		.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
		.pNext = NULL,
		.waitSemaphoreCount = 1,
		.pWaitSemaphores = &global.vkSemaphore1,
		.pWaitDstStageMask = &waitDstStageMask,
		.commandBufferCount = 1,
		.pCommandBuffers = &global.vkCommandBuffer,
		.signalSemaphoreCount = 1,
		.pSignalSemaphores = &global.vkSemaphore2
	};
	vkResult = vkQueueSubmit(global.vkQueue, 1, &vkSubmitInfo, 0);

	VkPresentInfoKHR vkPresentInfo = {
		.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
		.pNext = NULL,
		.waitSemaphoreCount = 1,
		.pWaitSemaphores = &global.vkSemaphore2,
		.swapchainCount = 1,
		.pSwapchains = &global.vkSwapchain,
		.pImageIndices = &imageIndex,
		.pResults = NULL
	};
	vkResult = vkQueuePresentKHR(global.vkQueue, &vkPresentInfo);

	vkResult = vkDeviceWaitIdle(global.vkDevice);
	vkFreeCommandBuffers(global.vkDevice, global.vkCommandPool, 1, &global.vkCommandBuffer);
}
