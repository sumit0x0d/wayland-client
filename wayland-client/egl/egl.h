#ifndef SIMPLE_WINDOW_EGL_H
#define SIMPLE_WINDOW_EGL_H

#include <EGL/egl.h>
#include <wayland-client.h>
#include <wayland-egl.h>

typedef struct {
	EGLDisplay eglDisplay;
	EGLContext eglContext;
	EGLSurface eglSurface;
	EGLConfig eglConfig;
} SW_Egl;

SW_Egl *SW_Egl_Create(EGLint interval, struct wl_display *wlDisplay, struct wl_egl_window *wlEglWindow);
void SW_Egl_Destroy(SW_Egl *egl);

#endif
