#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "egl.h"

SW_Egl *SW_Egl_Create(EGLint interval, struct wl_display *wlDisplay, struct wl_egl_window *wlEglWindow)
{
	SW_Egl *egl = (SW_Egl *)malloc(sizeof (SW_Egl));
	assert(egl);

	EGLint numConfig;
	EGLint major, minor;
	EGLint contextAttributes[] = {EGL_CONTEXT_CLIENT_VERSION, 2, EGL_NONE};
	EGLint configAttributes[] = {
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_RED_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_BLUE_SIZE, 8,
		EGL_ALPHA_SIZE, 8,
		EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
		EGL_NONE
	};
	EGLint surfaceAttributes[] = {EGL_NONE};

	// egl->eglDisplay = eglGetPlatformDisplayEXT(EGL_PLATFORM_WAYLAND_KHR, wlDisplay, configAttributes);
	egl->eglDisplay = eglGetDisplay(wlDisplay);
	if (egl->eglDisplay == EGL_NO_DISPLAY) {
		printf("eglGetDisplay\n");
		abort();
	}

	eglSwapInterval(egl->eglDisplay, interval);
	eglBindAPI(EGL_OPENGL_ES_API);
	eglInitialize(egl->eglDisplay, &major, &minor);
	printf("EGL major: %d, minor %d\n", major, minor);

	eglGetConfigs(egl->eglDisplay, NULL, 0, &numConfig);
	printf("EGL has %d configs\n", numConfig);
	if (eglChooseConfig(egl->eglDisplay, configAttributes, &egl->eglConfig, 1, &numConfig) != EGL_TRUE) {
		printf("eglChooseConfig\n");
		abort();
	}
	egl->eglContext = eglCreateContext(egl->eglDisplay, egl->eglConfig, EGL_NO_CONTEXT, contextAttributes);
	if (egl->eglContext == EGL_NO_CONTEXT) {
		abort();
	}

	egl->eglSurface = eglCreateWindowSurface(egl->eglDisplay, egl->eglConfig, (EGLNativeWindowType)wlEglWindow,
		surfaceAttributes);
	if (egl->eglSurface == EGL_NO_SURFACE) {
		abort();
	}

	if (!eglMakeCurrent(egl->eglDisplay, egl->eglSurface, egl->eglSurface, egl->eglContext)) {
		printf("eglMakeCurrent()\n");
		abort();
	}
	return egl;
}

void SW_Egl_Destroy(SW_Egl *egl)
{
	eglDestroyContext(egl->eglDisplay, egl->eglContext);
	eglDestroySurface(egl->eglDisplay, egl->eglSurface);
	eglTerminate(egl->eglDisplay);
}
