#ifndef SIMPLE_WINDOW_WAYLAND_CLIENT_H
#define SIMPLE_WINDOW_WAYLAND_CLIENT_H

#include <stdint.h>

#include <wayland-client.h>
#include <wayland-egl.h>

typedef struct {
	struct wl_display *wlDisplay;
	struct wl_egl_window *wlEglWindow;
	struct wl_surface *wlSurface;
} SW_WaylandClient;

SW_WaylandClient *SW_WaylandClient_Create(int32_t width, int32_t height, const char *title);
void SW_WaylandClient_Destroy(SW_WaylandClient *waylandClient);

#endif
