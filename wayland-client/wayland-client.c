#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>

#include <linux/input-event-codes.h>

#include <wayland-client.h>
#include <wayland-cursor.h>
#include <wayland-egl.h>

#include "wayland-client.h"

#include "presentation-time-client-protocol/presentation-time-client-protocol.h"
#include "viewporter-client-protocol/viewporter-client-protocol.h"
#include "xdg-decoration-unstable-v1-client-protocol/xdg-decoration-unstable-v1-client-protocol.h"
#include "xdg-shell-client-protocol/xdg-shell-client-protocol.h"


typedef struct {
	struct wl_registry *wlRegistry;

	struct wl_compositor *wlCompositor;
	struct wl_shm *wlShm;
	struct wl_output *wlOutput;
	struct wl_seat *wlSeat;
	struct wl_data_device_manager *wlDataDeviceManager;
	struct wl_subcompositor *wlSubcompositor;

	struct wl_keyboard *wlKeyboard;
	struct wl_pointer *wlPointer;
	struct wl_touch *wlTouch;
	struct wl_data_device *wlDataDevice;
	struct wl_data_source *wlDataSource;

	struct wp_presentation *wpPresentation;

	struct wp_viewporter *wpViewporter;

	struct xdg_wm_base *xdgWmBase;
	struct xdg_surface *xdgSurface;
	struct xdg_toplevel *xdgToplevel;

	struct wl_cursor_theme *wlCursorTheme;
	struct wl_cursor *wlCursor;
	struct wl_surface *wlCursorSurface;
	struct wl_buffer *wlCursorBuffer;
} GlobalContext;

static GlobalContext global;

static void wlRegistryGlobal(void *data, struct wl_registry *wlRegistry, uint32_t name, const char *interface,
	uint32_t version);
static void wlRegistryGlobalRemove(void *data, struct wl_registry *wlRegistry, uint32_t name);

static void wlShmFormat(void *data, struct wl_shm *wlShm, uint32_t format);

static void wlSeatCapabilities(void *data, struct wl_seat *wlSeat, uint32_t capabilities);
static void wlSeatName(void *data, struct wl_seat *wlSeat, const char *name);

static void wlOutputGeometry(void *data, struct wl_output *wlOutput, int32_t x, int32_t y, int32_t physicalWidth,
	int32_t physicalHeight, int32_t subpixel, const char *make, const char *model, int32_t transform);
static void wlOutputGeometry(void *data, struct wl_output *wlOutput, int32_t x, int32_t y, int32_t physicalWidth,
	int32_t physicalHeight, int32_t subpixel, const char *make, const char *model, int32_t transform);
static void wlOutputMode(void *data, struct wl_output *wlOutput, uint32_t flags, int32_t width, int32_t height,
	int32_t refresh);
static void wlOutputDone(void *data, struct wl_output *wlOutput);
static void wlOutputScale(void *data, struct wl_output *wlOutput, int32_t factor);
static void wlOutputName(void *data, struct wl_output *wlOutput, const char *name);
static void wlOutputDescription(void *data, struct wl_output *wlOutput, const char *description);

static void wlPointerEnter(void *data, struct wl_pointer *wlPointer, uint32_t serial, struct wl_surface *wlSurface,
	wl_fixed_t surfaceX, wl_fixed_t surfaceY);
static void wlPointerLeave(void *data, struct wl_pointer *wlPointer, uint32_t serial,
	struct wl_surface *wlSurface);
static void wlPointerMotion(void *data, struct wl_pointer *wlPointer, uint32_t time, wl_fixed_t surfaceX,
	wl_fixed_t surfaceY);
static void wlPointerButton(void *data, struct wl_pointer *wlPointer, uint32_t serial, uint32_t time,
	uint32_t button, uint32_t state);
static void wlPointerAxis(void *data, struct wl_pointer *wlPointer, uint32_t time, uint32_t axis,
	wl_fixed_t value);
static void wlPointerFrame(void *data, struct wl_pointer *wlPointer);
static void wlPointerAxisSource(void *data, struct wl_pointer *wlPointer, uint32_t axisSource);
static void wlPointerAxisStop(void *data, struct wl_pointer *wlPointer, uint32_t time, uint32_t axis);
static void wlPointerAxisDiscrete(void *data, struct wl_pointer *wlPointer, uint32_t axis, int32_t discrete);

static void wlKeyboardKeymap(void *data, struct wl_keyboard *wlKeyboard, uint32_t format, int32_t fd, uint32_t size);
static void wlKeyboardEnter(void *data, struct wl_keyboard *wlKeyboard, uint32_t serial,
	struct wl_surface *wlSurface, struct wl_array *keys);
static void wlKeyboardLeave(void *data, struct wl_keyboard *wlKeyboard, uint32_t serial,
	struct wl_surface *wlSurface);
static void wlKeyboardKey(void *data, struct wl_keyboard *wlKeyboard, uint32_t serial, uint32_t time, uint32_t key,
	uint32_t state);
static void wlKeyboardModifiers(void *data, struct wl_keyboard *wlKeyboard, uint32_t serial,
	uint32_t modsDepressed, uint32_t modsLatched, uint32_t modsLocked, uint32_t group);
static void wlKeyboardRepeatInfo(void *data, struct wl_keyboard *wlKeyboard, int32_t rate, int32_t delay);

static void wlTouchDown(void *data, struct wl_touch *wlTouch, uint32_t serial, uint32_t time,
	struct wl_surface *surface, int32_t id, wl_fixed_t x,wl_fixed_t y);
static void wlTouchUp(void *data, struct wl_touch *wlTouch, uint32_t serial, uint32_t time, int32_t id);
static void wlTouchMotion(void *data, struct wl_touch *wlTouch, uint32_t time, int32_t id, wl_fixed_t x,
	wl_fixed_t y);
static void wlTouchFrame(void *data, struct wl_touch *wlTouch);
static void wlTouchCancel(void *data, struct wl_touch *wlTouch);
static void wlTouchShape(void *data, struct wl_touch *wlTouch, int32_t id, wl_fixed_t major, wl_fixed_t minor);
static void wlTouchOrientation(void *data, struct wl_touch *wlTouch, int32_t id, wl_fixed_t orientation);

static void wlDataSourceTarget(void *data, struct wl_data_source *wlDataSource, const char *mimeType);
static void wlDataSourceSend(void *data, struct wl_data_source *wlDataSource, const char *mimeType, int32_t fd);
static void wlDataSourceCancelled(void *data, struct wl_data_source *wlDataSource);
static void wlDataSourceDndDropPerformed(void *data, struct wl_data_source *wlDataSource);
static void wlDataSourceDndFinished(void *data, struct wl_data_source *wlDataSource);
static void wlDataSourceAction(void *data, struct wl_data_source *wlDataSource, uint32_t dndAction);

static void wlDataOfferOffer(void *data, struct wl_data_offer *wlDataOffer, const char *mimeType);
static void wlDataOfferSourceActions(void *data, struct wl_data_offer *wlDataOffer, uint32_t sourceActions);
static void wlDataOfferAction(void *data, struct wl_data_offer *wlDataOffer, uint32_t dndAction);

static void wlDataDeviceDataOffer(void *data, struct wl_data_device *wlDataDevice, struct wl_data_offer *wlDataOffer);
static void wlDataDeviceEnter(void *data, struct wl_data_device *wlDataDevice, uint32_t serial,
	struct wl_surface *wlSurface, wl_fixed_t x, wl_fixed_t y, struct wl_data_offer *id);
static void wlDataDeviceLeave(void *data, struct wl_data_device *wlDataDevice);
static void wlDataDeviceMotion(void *data, struct wl_data_device *wlDataDevice, uint32_t time, wl_fixed_t x,
	wl_fixed_t y);
static void wlDataDeviceDrop(void *data, struct wl_data_device *wlDataDevice);
static void wlDataDeviceSelection(void *data, struct wl_data_device *wlDataDevice, struct wl_data_offer *wlDataOffer);

static void wpPresentationClockId(void *data, struct wp_presentation *wpPresentation, uint32_t clk_id);

static void xdgWmBasePing(void *data, struct xdg_wm_base *xdgWmBase, uint32_t serial);

static void xdgSurfaceConfigure(void *data, struct xdg_surface *xdgSurface, uint32_t serial);

static void xdgToplevelConfigure(void *data, struct xdg_toplevel *xdgToplevel, int32_t width, int32_t height,
	struct wl_array *states);
static void xdgToplevelClose(void *data, struct xdg_toplevel *xdgToplevel);
static void xdgToplevelConfigureBounds(void *data, struct xdg_toplevel *xdgToplevel, int32_t width, int32_t height);
static void xdgToplevelWmCapabilities(void *data, struct xdg_toplevel *xdgToplevel, struct wl_array *capabilities);

SW_WaylandClient *SW_WaylandClient_Create(int32_t width, int32_t height, const char *title)
{
	SW_WaylandClient *waylandClient = NULL;

	waylandClient = (SW_WaylandClient *)malloc(sizeof (SW_WaylandClient));
	assert(waylandClient);

	waylandClient->wlDisplay = wl_display_connect(NULL);
	assert(waylandClient->wlDisplay);

	global.wlRegistry = wl_display_get_registry(waylandClient->wlDisplay);
	static const struct wl_registry_listener wlRegistryListener = {
		.global = wlRegistryGlobal,
		.global_remove = wlRegistryGlobalRemove
	};
	wl_registry_add_listener(global.wlRegistry, &wlRegistryListener, NULL);

	wl_display_dispatch(waylandClient->wlDisplay);
	wl_display_roundtrip(waylandClient->wlDisplay);

	waylandClient->wlSurface = wl_compositor_create_surface(global.wlCompositor);
	waylandClient->wlEglWindow = wl_egl_window_create(waylandClient->wlSurface, width, height);

	global.wlDataDevice = wl_data_device_manager_get_data_device(global.wlDataDeviceManager, global.wlSeat);
	static const struct wl_data_device_listener wlDataDeviceListener = {
		.data_offer = wlDataDeviceDataOffer,
		.enter = wlDataDeviceEnter,
		.leave = wlDataDeviceLeave,
		.motion = wlDataDeviceMotion,
		.drop = wlDataDeviceDrop,
		.selection = wlDataDeviceSelection
	};
	wl_data_device_add_listener(global.wlDataDevice, &wlDataDeviceListener, NULL);

	global.wlDataSource = wl_data_device_manager_create_data_source(global.wlDataDeviceManager);
	static const struct wl_data_source_listener wlDataSourceListener = {
		.target = wlDataSourceTarget,
		.send = wlDataSourceSend,
		.cancelled = wlDataSourceCancelled,
		.dnd_drop_performed = wlDataSourceDndDropPerformed,
		.dnd_finished = wlDataSourceDndFinished,
		.action = wlDataSourceAction
	};
	wl_data_source_add_listener(global.wlDataSource, &wlDataSourceListener, NULL);

	wl_data_source_offer(global.wlDataSource, "text/plain");
	wl_data_source_offer(global.wlDataSource, "text/html");

	global.xdgSurface = xdg_wm_base_get_xdg_surface(global.xdgWmBase, waylandClient->wlSurface);
	static const struct xdg_surface_listener xdgSurfaceListener = {
		.configure = xdgSurfaceConfigure
	};
	xdg_surface_add_listener(global.xdgSurface, &xdgSurfaceListener, NULL);

	global.xdgToplevel = xdg_surface_get_toplevel(global.xdgSurface);
	static const struct xdg_toplevel_listener xdgToplevelListener = {
		.configure = xdgToplevelConfigure,
		.close = xdgToplevelClose,
		.configure_bounds = xdgToplevelConfigureBounds,
		.wm_capabilities = xdgToplevelWmCapabilities
	};
	xdg_toplevel_add_listener(global.xdgToplevel, &xdgToplevelListener, NULL);

	xdg_toplevel_set_title(global.xdgToplevel, title);
	xdg_toplevel_set_app_id(global.xdgToplevel, title);

	// struct wl_region *wlRegion = wl_compositor_create_region(global.wlCompositor);
	// wl_region_add(wlRegion, 0, 0, width, height);
	// wl_surface_set_opaque_region(waylandClient->wlSurface, wlRegion);

	// struct wl_surface *wlSubsurfaceSurface = wl_compositor_create_surface(global.wlCompositor);
	// struct wl_subsurface *wlSubsurface = wl_subcompositor_get_subsurface(global.wlSubcompositor,
	// 	wlSubsurfaceSurface, waylandClient->wlSurface);
	// wl_subsurface_set_sync(wlSubsurface);
	// wl_subsurface_place_above(wlSubsurface, wlSubsurfaceSurface);

	return waylandClient;
}

void SW_WaylandClient_Destroy(SW_WaylandClient *waylandClient)
{
	xdg_surface_destroy(global.xdgSurface);
	xdg_toplevel_destroy(global.xdgToplevel);
	wl_data_source_destroy(global.wlDataSource);
	wl_data_device_destroy(global.wlDataDevice);
	wl_data_device_manager_destroy(global.wlDataDeviceManager);
	wl_shm_destroy(global.wlShm);
	wl_buffer_destroy(global.wlCursorBuffer);
	wl_surface_destroy(global.wlCursorSurface);
	wl_cursor_theme_destroy(global.wlCursorTheme);
	wl_pointer_destroy(global.wlPointer);
	wl_keyboard_destroy(global.wlKeyboard);
	wl_touch_destroy(global.wlTouch);
	wl_seat_destroy(global.wlSeat);
	wl_compositor_destroy(global.wlCompositor);
	wl_registry_destroy(global.wlRegistry);
	wl_egl_window_destroy(waylandClient->wlEglWindow);
	wl_surface_destroy(waylandClient->wlSurface);
	wl_display_disconnect(waylandClient->wlDisplay);
	free(waylandClient);
}

static void wlRegistryGlobal(void *data, struct wl_registry *wlRegistry, uint32_t name, const char *interface,
	uint32_t version)
{
	if (strcmp(interface, wl_compositor_interface.name) == 0) {
		global.wlCompositor = (struct wl_compositor *)wl_registry_bind(wlRegistry, name,
			&wl_compositor_interface, version);
	} else if (strcmp(interface, wl_shm_interface.name) == 0) {
		global.wlShm = (struct wl_shm *)wl_registry_bind(wlRegistry, name, &wl_shm_interface, version);
		static struct wl_shm_listener wlShmListener = {
			.format = wlShmFormat
		};
		wl_shm_add_listener(global.wlShm, &wlShmListener, NULL);
	} else if (strcmp(interface, wl_output_interface.name) == 0) {
		global.wlOutput = (struct wl_output *)wl_registry_bind(wlRegistry, name, &wl_output_interface, version);
		static struct wl_output_listener wlOutputListener = {
			.geometry = wlOutputGeometry,
			.mode = wlOutputMode,
			.done = wlOutputDone,
			.scale = wlOutputScale,
			.name = wlOutputName,
			.description = wlOutputDescription
		};
		wl_output_add_listener(global.wlOutput, &wlOutputListener,  NULL);
	} else if (strcmp(interface, wl_data_device_manager_interface.name) == 0) {
		global.wlDataDeviceManager =  (struct wl_data_device_manager *)wl_registry_bind(wlRegistry, name,
			&wl_data_device_manager_interface, version);
	} else if (strcmp(interface, wl_subcompositor_interface.name) == 0) {
		global.wlSubcompositor = (struct wl_subcompositor *)wl_registry_bind(wlRegistry, name,
			&wl_subcompositor_interface, version);
	} else if (strcmp(interface, wl_seat_interface.name) == 0) {
		global.wlSeat = (struct wl_seat *)wl_registry_bind(wlRegistry, name, &wl_seat_interface, version);
		static struct wl_seat_listener wlSeatListener = {
			.capabilities = wlSeatCapabilities,
			.name = wlSeatName
		};
		wl_seat_add_listener(global.wlSeat, &wlSeatListener, NULL);
	} else if (strcmp(interface, xdg_wm_base_interface.name) == 0) {
		global.xdgWmBase = (struct xdg_wm_base *)wl_registry_bind(wlRegistry, name, &xdg_wm_base_interface,
			version);
		static const struct xdg_wm_base_listener xdgWmBaseListener = {
			.ping = xdgWmBasePing
		};
		xdg_wm_base_add_listener(global.xdgWmBase, &xdgWmBaseListener, NULL);
	} else if (strcmp(interface,  wp_presentation_interface.name) == 0) {
		global.wpPresentation = (struct wp_presentation *)wl_registry_bind(wlRegistry, name, &wp_presentation_interface,
			version);
		static const struct wp_presentation_listener wpPresentationListener = {
			.clock_id = wpPresentationClockId
		};
		wp_presentation_add_listener(global.wpPresentation, &wpPresentationListener, NULL);
	} else if (strcmp(interface,  wp_viewporter_interface.name) == 0) {
		global.wpViewporter = (struct wp_viewporter *)wl_registry_bind(wlRegistry, name, &wp_viewporter_interface,
			version);
	}

	printf("[wlRegistryGlobal] interface: %s | name: %d\n", interface, name);
}

static void wlRegistryGlobalRemove(void *data, struct wl_registry *wlRegistry, uint32_t name)
{}

static void wlShmFormat(void *data, struct wl_shm *wlShm, uint32_t format)
{}

static void wlOutputGeometry(void *data, struct wl_output *wlOutput, int32_t x, int32_t y, int32_t physicalWidth,
	int32_t physicalHeight, int32_t subpixel, const char *make, const char *model, int32_t transform)
{
	printf("[wlOutputGeometry] x: %d\n", x);
	printf("[wlOutputGeometry] y: %d\n", y);
	printf("[wlOutputGeometry] physicalWidth: %d\n", physicalWidth);
	printf("[wlOutputGeometry] physicalHeight: %d\n", physicalHeight);
	printf("[wlOutputGeometry] subpixel: %d\n", subpixel);
	printf("[wlOutputGeometry] make: %s\n", make);
	printf("[wlOutputGeometry] model: %s\n", model);
}

static void wlOutputMode(void *data, struct wl_output *wlOutput, uint32_t flags, int32_t width, int32_t height,
	int32_t refresh)
{
	printf("[wlOutputMode] width: %d\n", width);
	printf("[wlOutputMode] height: %d\n", height);
	double dots = width * height;
	// double dots_per_mm = dots / ((struct output_t *)data)->size;
	// double dots_per_in = dots_per_mm / 0.155;
	// printf("dpi: %.1f\n", dots_per_in);
}

static void wlOutputDone(void *data, struct wl_output *wlOutput)
{}

static void wlOutputScale(void *data, struct wl_output *wlOutput, int32_t factor)
{
	printf("scale: %d\n", factor);
}

static void wlOutputName(void *data, struct wl_output *wlOutput, const char *name)
{}

static void wlOutputDescription(void *data, struct wl_output *wlOutput, const char *description)
{}

static void wlSeatCapabilities(void *data, struct wl_seat *wlSeat, uint32_t capabilities)
{
	if (capabilities & WL_SEAT_CAPABILITY_POINTER) {
		global.wlPointer = wl_seat_get_pointer(global.wlSeat);
		static const struct wl_pointer_listener wlPointerListener = {
			.enter = wlPointerEnter,
			.leave = wlPointerLeave,
			.motion = wlPointerMotion,
			.button = wlPointerButton,
			.axis = wlPointerAxis,
			.frame = wlPointerFrame,
			.axis_source = wlPointerAxisSource,
			.axis_stop = wlPointerAxisStop,
			.axis_discrete = wlPointerAxisDiscrete
		};
		wl_pointer_add_listener(global.wlPointer, &wlPointerListener, NULL);
	}
	if (capabilities & WL_SEAT_CAPABILITY_KEYBOARD) {
		global.wlKeyboard = wl_seat_get_keyboard(global.wlSeat);
		static const struct wl_keyboard_listener wlKeyboardListener = {
			.keymap = wlKeyboardKeymap,
			.enter = wlKeyboardEnter,
			.leave = wlKeyboardLeave,
			.key = wlKeyboardKey,
			.modifiers = wlKeyboardModifiers,
			.repeat_info = wlKeyboardRepeatInfo
		};
		wl_keyboard_add_listener(global.wlKeyboard, &wlKeyboardListener, NULL);
	}
	if (capabilities & WL_SEAT_CAPABILITY_TOUCH) {
		global.wlTouch = wl_seat_get_touch(global.wlSeat);
		static const struct wl_touch_listener wlTouchListener = {
			.down = wlTouchDown,
			.up = wlTouchUp,
			.motion = wlTouchMotion,
			.frame = wlTouchFrame,
			.cancel = wlTouchCancel,
			.shape = wlTouchShape,
			.orientation = wlTouchOrientation
		};
		wl_touch_add_listener(global.wlTouch, &wlTouchListener, NULL);
	}
}

static void wlSeatName(void *data, struct wl_seat *wlSeat, const char *name)
{}

static void wlPointerEnter(void *data, struct wl_pointer *wlPointer, uint32_t serial, struct wl_surface *wlSurface,
	wl_fixed_t surfaceX, wl_fixed_t surfaceY)
{
	global.wlCursorTheme = wl_cursor_theme_load(NULL, 24, global.wlShm);
	global.wlCursor = wl_cursor_theme_get_cursor(global.wlCursorTheme, "left_ptr");
	global.wlCursorSurface = wl_compositor_create_surface(global.wlCompositor);
	global.wlCursorBuffer = wl_cursor_image_get_buffer(global.wlCursor->images[0]);
	wl_surface_attach(global.wlCursorSurface, global.wlCursorBuffer, 0, 0);
	wl_surface_damage(global.wlCursorSurface, 0, 0, global.wlCursor->images[0]->width,
		global.wlCursor->images[0]->height);
	wl_surface_commit(global.wlCursorSurface);
	wl_pointer_set_cursor(wlPointer, serial, global.wlCursorSurface,
		global.wlCursor->images[0]->hotspot_x, global.wlCursor->images[0]->hotspot_y);
}

static void wlPointerLeave(void *data, struct wl_pointer *wlPointer, uint32_t serial,
	struct wl_surface *wlSurface)
{
	// wl_cursor_theme_destroy(global.wlCursorTheme);
}

static void wlPointerMotion(void *data, struct wl_pointer *wlPointer, uint32_t time, wl_fixed_t surfaceX,
	wl_fixed_t surfaceY)
{
}

static void wlPointerButton(void *data, struct wl_pointer *wlPointer, uint32_t serial, uint32_t time,
	uint32_t button, uint32_t state)
{
	if (button == BTN_LEFT && state == WL_POINTER_BUTTON_STATE_PRESSED) {
		xdg_toplevel_move(global.xdgToplevel, global.wlSeat, serial);
	}
}

static void wlPointerAxis(void *data, struct wl_pointer *wlPointer, uint32_t time, uint32_t axis,
	wl_fixed_t value)
{
}

static void wlPointerFrame(void *data, struct wl_pointer *wlPointer)
{
}

static void wlPointerAxisSource(void *data, struct wl_pointer *wlPointer, uint32_t axisSource)
{
}

static void wlPointerAxisStop(void *data, struct wl_pointer *wlPointer, uint32_t time, uint32_t axis)
{
}

static void wlPointerAxisDiscrete(void *data, struct wl_pointer *wlPointer, uint32_t axis, int32_t discrete)
{
}
static void wlKeyboardKeymap(void *data, struct wl_keyboard *wlKeyboard, uint32_t format, int32_t fd, uint32_t size)
{
}

static void wlKeyboardEnter(void *data, struct wl_keyboard *wlKeyboard, uint32_t serial,
	struct wl_surface *wlSurface, struct wl_array *keys)
{
	wl_data_device_set_selection(global.wlDataDevice, global.wlDataSource, serial);
}

static void wlKeyboardLeave(void *data, struct wl_keyboard *wlKeyboard, uint32_t serial,
	struct wl_surface *wlSurface)
{}

static void wlKeyboardKey(void *data, struct wl_keyboard *wlKeyboard, uint32_t serial, uint32_t time, uint32_t key,
	uint32_t state)
{}

static void wlKeyboardModifiers(void *data, struct wl_keyboard *wlKeyboard, uint32_t serial,
	uint32_t modsDepressed, uint32_t modsLatched, uint32_t modsLocked, uint32_t group)
{}

static void wlKeyboardRepeatInfo(void *data, struct wl_keyboard *wlKeyboard, int32_t rate, int32_t delay)
{}

static void wlTouchDown(void *data, struct wl_touch *wlTouch, uint32_t serial, uint32_t time,
	struct wl_surface *surface, int32_t id, wl_fixed_t x,wl_fixed_t y)
{}

static void wlTouchUp(void *data, struct wl_touch *wlTouch, uint32_t serial, uint32_t time, int32_t id)
{}

static void wlTouchMotion(void *data, struct wl_touch *wlTouch, uint32_t time, int32_t id, wl_fixed_t x,
	wl_fixed_t y)
{}

static void wlTouchFrame(void *data, struct wl_touch *wlTouch)
{}

static void wlTouchCancel(void *data, struct wl_touch *wlTouch)
{}

static void wlTouchShape(void *data, struct wl_touch *wlTouch, int32_t id, wl_fixed_t major, wl_fixed_t minor)
{}

static void wlTouchOrientation(void *data, struct wl_touch *wlTouch, int32_t id, wl_fixed_t orientation)
{}

static const char text[] = "**Hello Wayland clipboard!**";
static const char html[] = "<strong>Hello Wayland clipboard!</strong>";

static void wlDataSourceTarget(void *data, struct wl_data_source *wlDataSource, const char *mimeType)
{}

static void wlDataSourceSend(void *data, struct wl_data_source *wlDataSource, const char *mimeType, int32_t fd)
{
	if (strcmp(mimeType, "text/plain") == 0) {
		write(fd, text, strlen(text));
	} else if (strcmp(mimeType, "text/html") == 0) {
		write(fd, html, strlen(html));
	} else {
		fprintf(stderr,
			"Destination client requested unsupported MIME type: %s\n",
			mimeType);
	}
	close(fd);
}

static void wlDataSourceCancelled(void *data, struct wl_data_source *wlDataSource)
{
}

static void wlDataSourceDndDropPerformed(void *data, struct wl_data_source *wlDataSource)
{
}

static void wlDataSourceDndFinished(void *data, struct wl_data_source *wlDataSource)
{
}

static void wlDataSourceAction(void *data, struct wl_data_source *wlDataSource, uint32_t dndAction)
{
}

static void wlDataOfferOffer(void *data, struct wl_data_offer *wlDataOffer, const char *mimeType)
{
}

static void wlDataOfferSourceActions(void *data, struct wl_data_offer *wlDataOffer, uint32_t sourceActions)
{
}

static void wlDataOfferAction(void *data, struct wl_data_offer *wlDataOffer, uint32_t dndAction)
{
}

static void wlDataDeviceDataOffer(void *data, struct wl_data_device *wlDataDevice, struct wl_data_offer *wlDataOffer)
{
	static const struct wl_data_offer_listener wlDataOfferListener = {
		.offer = wlDataOfferOffer,
		.source_actions = wlDataOfferSourceActions,
		.action = wlDataOfferAction
	};
	wl_data_offer_add_listener(wlDataOffer, &wlDataOfferListener, NULL);
}

static void wlDataDeviceEnter(void *data, struct wl_data_device *wlDataDevice, uint32_t serial,
	struct wl_surface *wlSurface, wl_fixed_t x, wl_fixed_t y, struct wl_data_offer *id)
{
}

static void wlDataDeviceLeave(void *data, struct wl_data_device *wlDataDevice)
{
}

static void wlDataDeviceMotion(void *data, struct wl_data_device *wlDataDevice, uint32_t time, wl_fixed_t x,
	wl_fixed_t y)
{
}

static void wlDataDeviceDrop(void *data, struct wl_data_device *wlDataDevice)
{
}

static void wlDataDeviceSelection(void *data, struct wl_data_device *wlDataDevice, struct wl_data_offer *wlDataOffer)
{
	// SimpleWaylandClient *client = (SimpleWaylandClient *)data;

	// if (dataOffer == NULL) {
	// 	printf("Clipboard is empty\n");
	// 	return;
	// }

	// int fds[2];
	// pipe(fds);
	// wl_data_offer_receive(dataOffer, "text/plain", fds[1]);
	// close(fds[1]);

	// wl_display_roundtrip(client->display);

	// printf("Clipboard data:\n");
	// while (1) {
		// char buf[1024];
		// ssize_t n = read(fds[0], buf, sizeof(buf));
		// if (n <= 0) {
		// 	break;
		// }
		// fwrite(buf, 1, n, stdout);
	// }
	// printf("\n");
	// close(fds[0]);

	// wl_data_offer_destroy(dataOffer);
}

static void wpPresentationClockId(void *data, struct wp_presentation *wpPresentation, uint32_t clk_id)
{}

static void xdgWmBasePing(void *data, struct xdg_wm_base *xdgWmBase, uint32_t serial)
{
	xdg_wm_base_pong(xdgWmBase, serial);
}

static void xdgSurfaceConfigure(void *data, struct xdg_surface *xdgSurface, uint32_t serial)
{
	xdg_surface_ack_configure(xdgSurface, serial);
}

static void xdgToplevelConfigure(void *data, struct xdg_toplevel *xdgToplevel, int32_t width, int32_t height,
	struct wl_array *states)
{
	printf("[xdgToplevelConfigure] width : %d, height : %d\n", width, height);
}

static void xdgToplevelClose(void *data, struct xdg_toplevel *xdgToplevel)
{}

static void xdgToplevelConfigureBounds(void *data, struct xdg_toplevel *xdgToplevel, int32_t width, int32_t height)
{}

static void xdgToplevelWmCapabilities(void *data, struct xdg_toplevel *xdgToplevel, struct wl_array *capabilities)
{}
