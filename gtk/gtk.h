#ifndef SIMPLE_WINDOW_GTK_H
#define SIMPLE_WINDOW_GTK_H

#include <stddef.h>
#include <stdint.h>

typedef struct {
} SW_Gtk;

SW_Gtk *SW_Gtk_Create(uint8_t *buffer, size_t size);
void SW_Gtk_Destroy(SW_Gtk *gtk);

void SW_Gtk_Encode(SW_Gtk *gtk);
void SW_Gtk_Dncode(SW_Gtk *gtk);

#endif
