#include <assert.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "wayland-client/egl/egl.h"
#include "wayland-client/wayland-client.h"

#include "gles/gles.h"
// #include "vulkan/vulkan.h"

#define WIDTH 1920
#define HEIGHT 1080
#define FPS 60

int main()
{
	FILE *file = fopen("../buffer2", "r");
	assert(file);

	void *jpegBuffer = malloc(WIDTH * HEIGHT * 4);
	assert(jpegBuffer);

	FILE *file2 = fopen("../buffer1", "r");
	assert(file2);

	void *jpegBuffer2 = malloc(WIDTH * HEIGHT * 4);
	assert(jpegBuffer2);

	size_t size1 = fread(jpegBuffer, 1, WIDTH * HEIGHT * 4, file);
	size_t size2 = fread(jpegBuffer2, 1, WIDTH * HEIGHT * 4, file2);

	const char *title = "Wayland Client";

	SW_WaylandClient *waylandClient = SW_WaylandClient_Create(WIDTH, HEIGHT, title);
	SW_Egl *egl = SW_Egl_Create(FPS, waylandClient->wlDisplay, waylandClient->wlEglWindow);
	SW_Gles_Create();
	// VulkanContext_Create(title, waylandClient->wlDisplay, waylandClient->wlSurface, WIDTH, HEIGHT);
	int count = 0;

	while (wl_display_dispatch(waylandClient->wlDisplay) != -1) {
		if (count++ % 2 == 0) {
			SW_Gles_Render(jpegBuffer, WIDTH, HEIGHT);
		} else {
			SW_Gles_Render(jpegBuffer2, WIDTH, HEIGHT);
		}
		eglSwapBuffers(egl->eglDisplay, egl->eglSurface);
		// VulkanContext_Render();
	}

	return EXIT_SUCCESS;
}
