#include <stdio.h>
#include <stdlib.h>

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include "gles.h"

static const char* vertexShaderSource = "#version 320 es\n"
	"layout (location = 0) in vec4 position;\n"
	"layout (location = 1) in vec2 texCoord;\n"
	"out vec2 vTexCoord;\n"
	"void main()\n"
	"{\n"
	"   gl_Position = position;\n"
	"   vTexCoord = texCoord;\n"
	"}\0";

static const char* fragmentShaderSource = "#version 320 es\n"
	"precision mediump float;\n"
	"uniform sampler2D uTex;\n"
	"layout (location = 0) out vec4 color;\n"
	"in vec2 vTexCoord;\n"
	"void main()\n"
	"{\n"
	"   color = texture(uTex, vTexCoord);\n"
	"}\n\0";

static GLuint vertexBuffer;
static GLuint indexBuffer;
static GLint vertexShader;
static GLint fragmentShader;
static GLuint shaderProgram;
static GLuint texture;
static GLuint uniformTex0;

void SW_Gles_Create()
{
	int result;

	unsigned int indices[] = {
		0, 1, 2,
		2, 3, 0
	};

	GLfloat vertices[] = {
		-1.0f, -1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 0.0f, 1.0f,
		1.0f,  1.0f, 0.0f, 0.0f,
		-1.0f,  1.0f, 1.0f, 0.0f
	};

	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof (GLfloat), (void *)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof (GLfloat), (void *)(2 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glGenBuffers(1, &indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader);
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE) {
		int len;
		glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &len);
		char *message = alloca(len * sizeof (char));
		glGetShaderInfoLog(vertexShader, len, &len, message);
		printf("vertex shader failed %s\n", message);
	}

	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE) {
		int len;
		glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &len);
		char *message = alloca(len * sizeof (char));
		glGetShaderInfoLog(fragmentShader, len, &len, message);
		printf("fragment shader failed %s\n", message);
	}

	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);
	glValidateProgram(shaderProgram);
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	glUseProgram(shaderProgram);

	glGenTextures(1, &texture);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	GLuint uniformTex0 = glGetUniformLocation(shaderProgram, "uTex");
	glUniform1i(texture, 0);
	glEnable(GL_BLEND);
}

void SW_Gles_Destroy()
{
}

void SW_Gles_Render(unsigned char *buffer, int width, int height)
{
	glClearColor(1.0, 1.0, 0.0, 1.0);
	glViewport(0, 0, width, height);
	glClear(GL_COLOR_BUFFER_BIT);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, buffer);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);
}
