#ifndef SIMPLE_WINDOW_GLES_H
#define SIMPLE_WINDOW_GLES_H

#include <GLES2/gl2.h>

void SW_Gles_Create();
void SW_Gles_Destroy();

void SW_Gles_Render(unsigned char *buffer, int width, int height);

#endif
