sudo apt install vulkan-validationlayers-dev libwayland-dev

wayland-scanner private-code < /usr/share/wayland-protocols/stable/viewporter/viewporter.xml > viewporter-client-protocol.c
wayland-scanner client-header < /usr/share/wayland-protocols/stable/viewporter/viewporter.xml > viewporter-client-protocol.h

wayland-scanner private-code < /usr/share/wayland-protocols/stable/presentation/presentation.xml > presentation-time-client-protocol.c
wayland-scanner client-header < /usr/share/wayland-protocols/stable/presentation/presentation.xml > presentation-time-client-protocol.h

wayland-scanner private-code < /usr/share/wayland-protocols/stable/xdg-shell/xdg-shell.xml > xdg-shell-client-protocol.c
wayland-scanner client-header < /usr/share/wayland-protocols/stable/xdg-shell/xdg-shell.xml > xdg-shell-client-protocol.h

wayland-scanner private-code < /usr/share/wayland-protocols/unstable/xdg-decoration/xdg-decoration-unstable-v1.xml > xdg-decoration-unstable-v1-client-protocol.c
wayland-scanner client-header < /usr/share/wayland-protocols/unstable/xdg-decoration/xdg-decoration-unstable-v1.xml > xdg-decoration-unstable-v1-client-protocol.h
